
PWD := $(shell pwd)
UNITY := /Applications/Unity/Unity.app/Contents/MacOS/Unity
DEPS := $(PWD)/deps
BASE := $(PWD)/base
BUNDLES := $(PWD)/bundles
BUILDS := $(PWD)/builds
SRC := $(BASE)/Assets/_SRC/


all: misc flat_roulette base_build

misc:
	mkdir -p $(DEPS)
	mkdir -p $(BUNDLES)
	mkdir -p $(BUILDS)

flat_roulette:
	git clone git@bitbucket.org:globalcasino/mobcas-roulette.git $(DEPS)/mobcas-roulette
	$(UNITY) -projectPath $(DEPS)/mobcas-roulette -executeMethod ExportAssetBundles.GetPrefab -nographics -batchmode -quit
	mv $(DEPS)/mobcas-roulette/bundle/*.unity3d $(BUNDLES)/
	mkdir $(SRC)/RouletteFlat/
	ln -s $(DEPS)/mobcas-roulette/Assets/_SRC/RouletteFlat/src $(SRC)/RouletteFlat/src


base_build:
	$(UNITY) -projectPath $(BASE) -executeMethod Build.BaseBuild -nographics -batchmode -quit
	mkdir $(BUILDS)/OSX/base.app/Contents/MacOS/bundles
	cp $(BUNDLES)/*.unity3d $(BUILDS)/OSX/base.app/Contents/MacOS/bundles/

	mkdir $(BUILDS)/WIN/bundles
	cp $(BUNDLES)/*.unity3d $(BUILDS)/WIN/bundles

	mkdir $(BUILDS)/LIN/bundles
	cp $(BUNDLES)/*.unity3d $(BUILDS)/LIN/bundles

clean:
	rm -rf $(SRC)/RouletteFlat
	rm -rf $(DEPS)
	rm -rf $(BUNDLES)
	rm -rf $(BUILDS)

.PHONY: misc flat_roulette base_clone base_build
