﻿using UnityEngine;
using System.Collections;

public static class UniWebSettings {
	public static int SendTimeout = 3000;
	public static int ReceiveTimeout = 5000;
}
