﻿#if UNITY_EDITOR

using UnityEngine;
using UnityEditor;
using System.Collections;
using System.IO;

public static class Build {

	
	public static void BaseBuild(){
		string[] levels  = new string[] {string.Format("Assets/_SRC/base/{0}/{0}.unity", MainSettings.LobbyName), "Assets/_SRC/base/Container/Container.unity"};
		if (!Directory.Exists("../builds/OSX/"))
			Directory.CreateDirectory("../builds/OSX/");
		BuildPipeline.BuildPlayer(levels, "../builds/OSX/base.app" , BuildTarget.StandaloneOSXUniversal, BuildOptions.None);

		if (!Directory.Exists("../builds/WIN/"))
			Directory.CreateDirectory("../builds/WIN/");
		BuildPipeline.BuildPlayer(levels, "../builds/WIN/base.exe" , BuildTarget.StandaloneWindows, BuildOptions.None);

		if (!Directory.Exists("../builds/LIN/"))
			Directory.CreateDirectory("../builds/LIN/");
		BuildPipeline.BuildPlayer(levels, "../builds/LIN/base" , BuildTarget.StandaloneLinux, BuildOptions.None);

	}
}

#endif
