﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SmartWeb : MonoBehaviour {


	public delegate void DSmartHttp(HTTP.Request req, HTTP.Response resp);
	public static SmartWeb Instance {get; protected set;}

	void Awake()
	{
		SmartWeb.Instance = this;
	}


	public IEnumerator POSTRequest(DSmartHttp callback, string url, params object[] args)
	{
		Dictionary<string, object> d = new Dictionary<string, object>();
		for(int i=0;i<args.Length;i+=2)
			d.Add(args[i].ToString(), args[i+1]);

		yield return POSTRequest(callback, url, d);
	}

	public IEnumerator POSTRequest(DSmartHttp callback, string url, Dictionary<string, object> args)
	{
		WWWForm w = new WWWForm();
		foreach(KeyValuePair<string, object> pair in args)
			w.AddField(pair.Key, pair.Value.ToString());
		HTTP.Request request = new HTTP.Request("POST", url);
		request.AddHeader("Content-Type", "application/json; charset=utf-8");

		request.Send();
		while(!request.isDone) yield return new WaitForEndOfFrame();

		if(request.exception != null) 
			callback.Invoke(request, null);
		else {
			var response = request.response;
			callback.Invoke(request, response);
		}
	}


	// Use this for initialization
	void Start () {
	}

	// Update is called once per frame
	void Update () {
	}
}
