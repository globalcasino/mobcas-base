using UnityEngine;
using System.Collections;

[AddComponentMenu ("2D Toolkit/GUI/tk2dSimpleAnimatedButton")]
public class tk2dSimpleAnimatedButton : MonoBehaviour
{
	public event System.EventHandler Click;

	public Camera viewCamera;

	public KeyCode buttonKeyPress;
	
	tk2dSpriteAnimator buttonAnimator;
	
	public string buttonDownAnimation = "";
	public string buttonUpAnimation = "";
	public string buttonPressedAnimation = "";
	public string buttonDisabledAnimation = "";

	public AudioClip buttonDownSound = null;
	public AudioClip buttonUpSound = null;
	public AudioClip buttonPressedSound = null;

	public bool buttonDisable = false;

	public bool ButtonDisable
	{
		get
		{
			return this.buttonDisable;
		}
		set
		{
			buttonDisable = value;
		}
	}


	public bool Enabled
	{
		get { return ! buttonDisable; }
		set { if (value) Enable (); else Disable (); }
	}

	public void Disable ()
	{
		this.buttonDisable = true;
		if (buttonDisabledAnimation != "")
		{
			buttonAnimator.Play(buttonDisabledAnimation);
		}
	}

	public void Enable ()
	{
		if (this.buttonDisable)
		{
			this.buttonDisable = false;
			if (buttonUpAnimation != "")
			{
				buttonAnimator.Play(buttonUpAnimation);
			}
		}
	}

	bool buttonPressed = false;

	public GameObject targetObject = null;
	/// <summary>
	/// The message to send to the object. This should be the name of the method which needs to be called.
	/// </summary>
	public string messageName = "";

	public bool clickByDown = false;

	// Use this for initialization
	void Start ()
	{
		if (viewCamera == null)
		{
			// Find a camera parent
			Transform node = transform;
			while (node && node.camera == null)
			{
				node = node.parent;
			}
			if (node && node.camera != null)
			{
				viewCamera = node.camera;
			}

			// See if a tk2dCamera exists
//			if (viewCamera == null && tk2dCamera.inst)
//			{
//				viewCamera = tk2dCamera.inst.mainCamera;
//			}

			// ...otherwise, use the main camera
			if (viewCamera == null)
			{
				viewCamera = Camera.main;
			}
		}

		viewCamera = Camera.main;

		buttonAnimator = GetComponent<tk2dSpriteAnimator>();
		
		RefreshSpriteId ();
		RefreshButton ();

		if (collider == null)
		{
			BoxCollider boxCollider = gameObject.AddComponent<BoxCollider> ();
			Vector3 size = boxCollider.size;
			size.z = 0.2f;
			boxCollider.size = size;
		}
	}

	void RefreshSpriteId ()
	{	/*
		if (sprite)
		{
			// Change this to use animated sprites if necessary
			// Same concept here, lookup Ids and call Play(xxx) instead of .spriteId = xxx
			if (buttonDownSprite.Length > 0)
				buttonDownSpriteId = sprite.GetSpriteIdByName (buttonDownSprite);
			if (buttonUpSprite.Length > 0)
				buttonUpSpriteId = sprite.GetSpriteIdByName (buttonUpSprite);
//			if (buttonPressedSprite.Length > 0)
//				buttonPressedSpriteId = sprite.GetSpriteIdByName (buttonPressedSprite);
			if (buttonDisabledSprite.Length > 0)
				buttonDisabledSpriteId = sprite.GetSpriteIdByName (buttonDisabledSprite);
		}
		*/
	}

	public void RefreshButton ()
	{
		RefreshSpriteId ();
		this.buttonDisable = ! this.buttonDisable;
		Enabled = this.buttonDisable;
	}

	void PlaySound (AudioClip source)
	{
		if (audio && source)
		{
			audio.PlayOneShot (source);
		}
	}

	void Down ()
	{
		if (buttonDownAnimation != "")
		{
			buttonAnimator.Play(buttonDownAnimation);
		}

		if (targetObject && messageName.Length > 0)
		{
			if (clickByDown)
			{
				if (!this.buttonDisable)
				{
					targetObject.SendMessage (messageName);
				}
			}
		}

		if (clickByDown && Click != null)
		{
			Debug.Log ("-------" + clickByDown);
			Click (this, new System.EventArgs ());
		}

		buttonPressed = true;
	}

	void Up ()
	{
		if (buttonUpAnimation != "")
		{
			buttonAnimator.Play(buttonUpAnimation);
		}

		if (targetObject && messageName.Length > 0)
		{
			if (!clickByDown)
			{
				if (!this.buttonDisable)
				{
					targetObject.SendMessage (messageName);
				}
			}
		}

		if ( ! clickByDown && Click != null)
		{
			Click (this, new System.EventArgs ());			
		}

		buttonPressed = false;
	}

	// Update is called once per frame
	void Update ()
	{
		if ( ! buttonDisable)
		{
			if (Input.GetKeyDown (buttonKeyPress))
			{
				Down ();
				return ;
			}

			if ( ! buttonPressed && Input.GetMouseButtonDown (0))
			{
				Ray ray = viewCamera.ScreenPointToRay (Input.mousePosition);
				RaycastHit hitInfo;
				if (collider.Raycast (ray, out hitInfo, 1.0e8f))
				{
					if (!Physics.Raycast (ray, hitInfo.distance - 0.01f))
					{
						Down ();
					}
				}
			}

			if ( !Input.GetMouseButton (0) && !Input.GetKeyDown(buttonKeyPress))
			{ 
				if (buttonPressed)
				{
					Up ();
				}
			}
		}
		else
		{
			if (buttonDisabledAnimation != "")
			{
				if(buttonAnimator.CurrentClip.name != buttonDisabledAnimation)
					buttonAnimator.Play(buttonDisabledAnimation);
			}

		}
	}


	// ------------------------------------------------------------------------

	public void SendPress ()
	{
		if (!buttonDisable)
		{
			Down ();
		}
	}
}
