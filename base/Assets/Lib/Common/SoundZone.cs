﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SoundZone : MonoBehaviour {

	[System.Serializable]
	public class Sound
	{
		public string name = "";
		public  AudioClip clip = null;
	}


	public static SoundZone Instance { get; protected set; }
	private Dictionary<string, AudioClip> audios = new Dictionary<string, AudioClip>();
	public SoundZoneSource prefab;

	public Sound[] sounds = new Sound[0];

	// Use this for initialization
	void Start () {
		SoundZone.Instance = this;
		foreach(Sound s in sounds)
			this.audios.Add(s.name, s.clip);
	}

	public void Play(params string[] names)
	{
		GameObject g = new GameObject();
		g.AddComponent<SoundZoneSource>();
		g.name = string.Format("Sound({0})", string.Join(",", names) );
		g.transform.parent = this.transform;
		SoundZoneSource p = g.GetComponent<SoundZoneSource>();

		List<AudioClip> ac = new List<AudioClip>();
		foreach(string name in names)
			ac.Add(audios[name]);
		p.Play(ac.ToArray());
	}

//	public void Play(string name)
//	{
//		GameObject g = new GameObject();
//		g.AddComponent<SoundZoneSource>();
//		g.name = string.Format("Sound({0})", name);
//		SoundZoneSource p = g.GetComponent<SoundZoneSource>();
//
//
//	}

	// Update is called once per frame
	void Update () {
	
	}
}
