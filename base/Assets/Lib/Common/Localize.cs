﻿using UnityEngine;
using System.Collections;
using _ = underscore.U;


[RequireComponent(typeof(tk2dTextMesh))]
public class Localize : MonoBehaviour {


	public static Languages currentLang = Languages.EN;
	private Languages lang = Languages.EN;
	public Languages textMeshLanguages = Languages.EN;
	public LocalizationString[] strings = new LocalizationString[1];
	private tk2dTextMesh textMesh;

	int IndexOf(Languages lang)
	{
		for(int i=0; i< this.strings.Length; i++)
			if (strings[i].lang == lang)
				return i;
		return IndexOf(textMeshLanguages);
	}

	// Use this for initialization
	void Start () {
		this.textMesh = this.GetComponent<tk2dTextMesh>();
		_.Add<LocalizationString>(ref strings, new LocalizationString(textMeshLanguages, textMesh.text));
	}
	
	// Update is called once per frame
	void Update () {
		if (currentLang != lang && this.textMesh!=null)
		{
			lang = currentLang;
			LocalizationString str = strings[IndexOf(lang)];
			textMesh.text = str.newLine;
			textMesh.Commit();
			textMesh.transform.localScale = new Vector3(str.scale, str.scale, 1);
		}
	}
}
