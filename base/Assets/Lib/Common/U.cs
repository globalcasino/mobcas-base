﻿// <copyright file="U.cs" company="Application Forge">
// Copyright (c) 2013 All Rights Reserved
// </copyright>
// <author>Dmitry Vysochin</author>
// <date>10/07/2013 11:39:58 AM </date>
// <summary>Реализация аналога модуля underscore.js</summary>

using System;
using System.Collections.Generic;

namespace underscore
{
	public delegate T DArrayItemOut<T>(T item);
	public delegate T DArrayItemOutContext<T>(T item, object context);
	
	public delegate void DArrayItem<T>(T item);
	public delegate void DArrayItemContext<T>(T item, object context);
	
	public delegate object DReduceItem<T>(object memo, T item, int key);
	public delegate object DReduceItemContext<T>(object memo, T item, int key, object context);
	
	public delegate void DIterator(int index);
	public delegate void DIteratorContext(int index, object context);
	
	/// <summary>
	/// Делегат совпадения объекта
	/// </summary>
	public delegate bool DFindItem<T>(T item);
	
	/// <summary>
	/// Делегат сравнения элемента A и Б
	/// </summary>
	public delegate int DCompareItem<T>(T a, T b);
	
	/// <summary>
	/// Делегат служит для того чтоб определять вес элемента
	/// </summary>
	public delegate float DItemPowerValue<T>(T a);
	
	
	public class U
	{
		
		/// <summary>
		/// Откинуть N первых элементов массива
		/// </summary>
		/// <param name="array">Array.</param>
		/// <param name="count">Count.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static void Rest<T>(ref T[] array, int count)
		{
			array = Rest (array, count);
		}
		
		/// <summary>
		/// Меняет элементы местами
		/// </summary>
		/// <param name="lhs">Lhs.</param>
		/// <param name="rhs">Rhs.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static void Swap<T>(ref T lhs, ref T rhs)
		{
			T temp;
			temp = lhs;
			lhs = rhs;
			rhs = temp;
		}
		
		/// <summary>
		/// Вернет все, кроме первого элементы массива. 
		/// Если передан count то вернет все элементы, позиция которых больше или равна index.
		/// </summary>
		/// <param name="array">Array.</param>
		/// <param name="count">Count.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static T[] Rest<T>(T[] array, int count)
		{
			count = Math.Min (array.Length, count);
			
			T[] tmp = new T[array.Length - count];
			Array.Copy (array, count, tmp, 0, tmp.Length);
			return tmp;
		}
		
		/// <summary>
		/// Отбросить один элемент массива сначала
		/// </summary>
		/// <param name="array">Array.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static void Rest<T>(ref T[] array)
		{
			Rest (ref array,1);
		}
		
		/// <summary>
		/// Отбросить один элемент массива сначала
		/// </summary>
		/// <param name="array">Array.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static T[] Rest<T>(T[] array)
		{
			return Rest (array,1);
		}
		
		/// <summary>
		/// Отбросить N последних элементов массива
		/// </summary>
		/// <param name="array">Array.</param>
		/// <param name="count">Count.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static T[] Initial<T>(T[] array, int count)
		{
			count = Math.Min (array.Length, count);
			
			T[] tmp = new T[array.Length - count];
			Array.Copy (array, 0, tmp, 0, tmp.Length);
			return tmp;
		}
		
		/// <summary>
		/// Отбросить N последних элементов массива
		/// </summary>
		/// <param name="array">Array.</param>
		/// <param name="count">Count.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static void Initial<T>(ref T[] array, int count)
		{
			array = Initial(array, count);
		}
		
		/// <summary>
		/// Вернет подмассив из всех элементов родительского, за исключением последнего.
		/// </summary>
		/// <param name="array">Array.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static T[] Initial<T>(T[] array)
		{
			return Initial (array, 1); 
		}
		
		/// <summary>
		/// Вернет подмассив из всех элементов родительского, за исключением последнего.
		/// </summary>
		/// <param name="array">Array.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static void Initial<T>(ref T[] array)
		{
			array = Initial(array, 1);
		}
		
		/// <summary>
		/// Вернет «перемешанную» копию массива
		/// </summary>
		/// <param name="array">Array.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static T[] Shuffle<T>(T[] array)
		{
			T[] tmp = Copy (array);
			System.Random rnd = new Random();
			for (int i = 0; i < tmp.Length; i++)
				Swap(ref tmp[i], ref tmp[rnd.Next(0, tmp.Length)]);
			return tmp;
		}
		
		/// <summary>
		/// Вернет «перемешанную» копию массива
		/// </summary>
		/// <param name="array">Array.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static void Shuffle<T>(ref T[] array)
		{
			System.Random rnd = new Random();
			for (int i = 0; i < array.Length; i++)
				Swap(ref array[i], ref array[rnd.Next(0, array.Length)]);
		}
		
		/// <summary>
		/// Перевести любой массив в строковый
		/// </summary>
		/// <returns>The string array.</returns>
		/// <param name="array">Array.</param>
		public static string[] ToStringArray(Array array)
		{
			string[] res = new string[array.Length];
			for (int i=0; i<res.Length; i++)
				res [i] = array.GetValue (i).ToString();
			return res;
		}
		
		/// <summary>
		/// Специальный метод красивого отображения массивов в консоль
		/// </summary>
		/// <param name="array">Array.</param>
		public static void Out(Array array)
		{
			Console.WriteLine(ToString(array));
		}
		
		/// <summary>
		/// Показать массив в виде строки
		/// </summary>
		/// <returns>Строка</returns>
		/// <param name="array">Массив</param>
		public static string ToString(Array array)
		{
			string res = "";
			foreach(string item in ToStringArray(array))
				res += item + "\t";
			
			res += "("+array.Length+")";
			return res;
		}
		
		#region Map Reduce
		
		/// <summary>
		/// Вернет новые массив, полученный преобразованием каждого элемента list в функции mapDelegate(T item)
		/// </summary>
		/// <param name="array">Array.</param>
		/// <param name="mapDelegate">(T item)</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static T[] Map<T>(T[] array, DArrayItemOut<T> mapDelegate)
		{
			T[] tmp = new T[array.Length];
			for (int i=0; i<tmp.Length; i++)
				tmp [i] = (T)mapDelegate.DynamicInvoke (array[i]);
			return tmp;
		}
		
		/// <summary>
		/// Вернет новые массив, полученный преобразованием каждого элемента list в функции mapDelegate(T item)
		/// </summary>
		/// <param name="array">Array.</param>
		/// <param name="mapDelegate">(T item)</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static void Map<T>(ref T[] array, DArrayItemOut<T> mapDelegate)
		{
			array = Map (array, mapDelegate);
		}
		
		
		/// <summary>
		/// Вернет новые массив, полученный преобразованием каждого элемента list в функции mapDelegate(T item)
		/// </summary>
		/// <param name="array">Array.</param>
		/// <param name="context">Context.</param>
		/// <param name="mapDelegate">(T item)</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static T[] Map<T>(T[] array, object context, DArrayItemOutContext<T> mapDelegate)
		{
			T[] tmp = new T[array.Length];
			for (int i=0; i<tmp.Length; i++)
				tmp [i] = (T)mapDelegate.DynamicInvoke (array[i], context);
			return tmp;
		}
		
		/// <summary>
		/// Вернет новые массив, полученный преобразованием каждого элемента list в функции mapDelegate(T item)
		/// </summary>
		/// <param name="array">Array.</param>
		/// <param name="context">Context.</param>
		/// <param name="mapDelegate">(T item)</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static void Map<T>(ref T[] array, object context, DArrayItemOutContext<T> mapDelegate)
		{
			array = Map (array, context, mapDelegate);
		}
		
		/// <summary>
		/// Проходит по всему списку элементов, вызывая для каждого из них функцию iterator, которая будет вызвана в 
		/// контексте context, если он был передан. 
		/// </summary>
		/// <param name="array">Array.</param>
		/// <param name="eachDelegate">Each delegate.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static void Each<T>(T[] array, DArrayItem<T> eachDelegate)
		{
			for (int i=0; i<array.Length; i++)
				eachDelegate.Invoke (array[i]);
		}
		
		/// <summary>
		/// Проходит по всему списку элементов, вызывая для каждого из них функцию iterator, которая будет вызвана в 
		/// контексте context, если он был передан. 
		/// </summary>
		/// <param name="array">Array.</param>
		/// <param name="contet">Contet.</param>
		/// <param name="eachDelegate">Each delegate.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static void Each<T>(T[] array, object context, DArrayItemContext<T> eachDelegate)
		{
			for (int i=0; i<array.Length; i++)
				eachDelegate.Invoke (array[i], context);
		}
		
		
		/// <summary>
		/// Reduce объеденит вместе значения из list, путем пропускания каждого из них через функцию iterator, 
		/// которая в качестве аргументов получит (object memo, T item, int key);. Где memo начальное значение шага 
		/// редукции, которое было 
		/// </summary>
		/// <param name="array">Array.</param>
		/// <param name="reduceDelegate">(object memo, T item, int key)</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static object Reduce<T>(T[] array, DReduceItem<T> reduceDelegate)
		{
			object last_memo = null;
			for (int i=0; i<array.Length; i++) {
				last_memo = reduceDelegate.Invoke (last_memo, array [i], i);
			}
			return last_memo;
		}
		
		/// <summary>
		/// Reduce объеденит вместе значения из list, путем пропускания каждого из них через функцию iterator, 
		/// которая в качестве аргументов получит (object memo, T item, int key, object context);. Где memo начальное значение шага 
		/// редукции, которое было 
		/// </summary>
		/// <param name="array">Array.</param>
		/// <param name="context">Context.</param>
		/// <param name="reduceDelegate">(object memo, T item, int key, object context)</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static object Reduce<T>(T[] array, object context, DReduceItemContext<T> reduceDelegate)
		{
			object last_memo = null;
			for (int i=0; i<array.Length; i++) {
				last_memo = reduceDelegate.Invoke (last_memo, array [i], i, context);
			}
			return last_memo;
		}
		
		#endregion 
		
		/// <summary>
		/// Вызывает для каждого элемента array, функцию сравнения findDelegate (item), возвращая первый элемент, для которого iterator вернула true
		/// </summary>
		/// <param name="array">Array.</param>
		/// <param name="findDelegate">(item)</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static T Find<T>(T[] array, DFindItem<T> findDelegate)
		{
			for (int i=0; i<array.Length; i++)
				if (findDelegate.Invoke(array[i]))
					return array[i];
			return default(T);
		}
		
		/// <summary>
		/// Проходит каждое значение array, возвращая массив из значений, для которых findDelegate (item) вернул true
		/// </summary>
		/// <param name="array">Array.</param>
		/// <param name="findDelegate">(item)</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static T[] Filter<T>(T[] array, DFindItem<T> findDelegate)
		{
			List<T> res = new List<T> ();
			
			for (int i=0; i<array.Length; i++) {
				if (findDelegate.Invoke (array[i]))
					res.Add (array[i]);
			}
			return res.ToArray();
		}
		
		/// <summary>
		/// Проходит каждое значение array, оставляя только те значения, для которых findDelegate (item) вернул true
		/// </summary>
		/// <param name="array">Array.</param>
		/// <param name="findDelegate">(item)</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static void Filter<T>(ref T[] array, DFindItem<T> findDelegate)
		{
			array = Filter (array, findDelegate);
		}
		
		/// <summary>
		/// Вернет копию массива array, из которой будут удалены все значения values. 
		/// </summary>
		/// <param name="array">Array.</param>
		/// <param name="item">Item.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static T[] Without<T>(T[] array, T item)
		{
			return Filter(array, (i) => { 
				if (EqualityComparer<T>.Default.Equals(i, item))
					return false;
				else
					return true;
			});
		}
		
		/// <summary>
		/// Вернет копию массива array, из которой будут удалены все значения values. 
		/// </summary>
		/// <param name="array">Array.</param>
		/// <param name="item">Item.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static void Without<T>(ref T[] array, T item)
		{
			array = Without(array, item);
		}
		
		/// <summary>
		/// Вернет копию массива array, из которой будут удалены все значения values. 
		/// </summary>
		/// <param name="array">Array.</param>
		/// <param name="items">Items.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static T[] Without<T>(T[] array, T[] items)
		{
			return Filter(array, (i) => { 
				foreach(T item in items)
					if (EqualityComparer<T>.Default.Equals(i, item))
						return false;
				
				return true;
			});
			
		}
		
		/// <summary>
		/// Вернет копию массива array, из которой будут удалены все значения values. 
		/// </summary>
		/// <param name="array">Array.</param>
		/// <param name="items">Items.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static void Without<T>(ref T[] array, T[] items)
		{
			array = Without (array, items);
		}
		
		
		/// <summary>
		/// Возвращает массив, содержащий все значения array, за исключением элементов, 
		/// для которых функция findDelegate (item) вернула значение, отличное от true. Т.е. reject 
		/// является «антонимом» filter
		/// </summary>
		/// <param name="array">Array.</param>
		/// <param name="findDelegate">(item)</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static T[] Reject<T>(T[] array, DFindItem<T> findDelegate)
		{
			List<T> res = new List<T> ();
			
			for (int i=0; i<array.Length; i++) {
				if (!findDelegate.Invoke (array[i]))
					res.Add (array[i]);
			}
			return res.ToArray();
		}
		
		/// <summary>
		/// Возвращает массив, содержащий все значения array, за исключением элементов, 
		/// для которых функция findDelegate (item) вернула значение, отличное от true. Т.е. reject 
		/// является «антонимом» filter
		/// </summary>
		/// <param name="array">Array.</param>
		/// <param name="findDelegate">(item)</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static void Reject<T>(ref T[] array, DFindItem<T> findDelegate)
		{
			array = Reject (array, findDelegate);
		}
		
		
		/// <summary>
		/// Вернет true, если для каждого значения из array findDelegate (item) вернет true. 
		/// Делегирует вызов к нативной реализации every, если она существует.
		/// </summary>
		/// <param name="array">Array.</param>
		/// <param name="findDelegate">(item)</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static bool All<T>(T[] array, DFindItem<T> findDelegate)
		{
			for (int i=0; i<array.Length; i++) 
				if (!findDelegate.Invoke (array[i]))
					return false;
			return true;
		}
		
		/// <summary>
		/// Вернет true, если хотябы для одного значения из array findDelegate (item) вернет true. 
		/// После нахождения первого, удовлетворяющего условию элемента выполнение функции 
		/// прерывается. Делегирует вызов к нативной реализации some, если она существует.
		/// </summary>
		/// <param name="array">Array.</param>
		/// <param name="findDelegate">(item)</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static bool Any<T>(T[] array, DFindItem<T> findDelegate)
		{
			for (int i=0; i<array.Length; i++) 
				if (findDelegate.Invoke (array[i]))
					return true;
			return false;
		}
		
		/// <summary>
		/// Вернет true, если хотябы для одного значения из arrray равно item. 
		/// </summary>
		/// <param name="array">Array.</param>
		/// <param name="item">Item.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static bool Any<T>(T[] array, T item)
		{
			return IndexOf<T>(array, item) > -1;
		}
		
		
		
		/// <summary>
		/// Вернет true, если в list содержится элемент, эквивалентный value. 
		/// </summary>
		/// <param name="array">Array.</param>
		/// <param name="value">Value.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static bool Include<T>(T[] array, T value)
		{
			for (int i=0; i<array.Length; i++) 
				if (EqualityComparer<T>.Default.Equals(array[i], value))
					return true;
			return false;
		}
		
		/// <summary>
		/// Наверное, наиболее частоиспользуемый метод для map: вернет массив, 
		/// состоящий из значений свойства propertyName каждого из элементов list.
		/// </summary>
		/// <param name="array">Array.</param>
		/// <param name="propertyName">Property name.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static object[] Pluck<T>(T[] array, string propertyName)
		{
			object[] res = new object[array.Length];
			for (int i=0; i<array.Length; i++)
				res[i] = array [i].GetType ().GetProperty (propertyName).GetValue (array[i], null);
			return res;
		}
		
		/// <summary>
		/// Вернет максимальное значение из list. compareDelegate, будет использован для 
		/// генерация критерия, по которому будет проведено сравнение
		/// </summary>
		/// <param name="array">Array.</param>
		/// <param name="compareDelegate">Compare delegate.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static T Max<T>(T[] array, DCompareItem<T> compareDelegate)
		{
			if (array.Length == 0)
				return default(T);
			
			T res = array [0];
			for (int i=1; i<array.Length; i++) 
				if (compareDelegate.Invoke(res, array[i]) > 0 ) 
					res = array [i];
			
			return res;
		}
		
		/// <summary>
		/// Вернет минимальное значение из list. compareDelegate, будет использован для 
		/// генерация критерия, по которому будет проведено сравнение
		/// </summary>
		/// <param name="array">Array.</param>
		/// <param name="compareDelegate">Compare delegate.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static T Min<T>(T[] array, DCompareItem<T> compareDelegate)
		{
			if (array.Length == 0)
				return default(T);
			
			T res = array [0];
			for (int i=1; i<array.Length; i++) 
				if (compareDelegate.Invoke(res, array[i]) < 0 ) 
					res = array [i];
			
			return res;
		}
		
		/// <summary>
		/// Определить позицию, куда value должен быть вставлен, чтобы не нарушился порядок отсортированного list.
		/// </summary>
		/// <returns>The index.</returns>
		/// <param name="array">Array.</param>
		/// <param name="item">Item.</param>
		/// <param name="compareDelegate">Compare delegate.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static int SortedIndex<T>(T[] array, T item, DCompareItem<T> compareDelegate)
		{
			throw new NotImplementedException();
		}
		
		/// <summary>
		/// Вернет отсортированную копию list, сортировка будет проведена по значению, возвращенному 
		/// функцией compareDelegate(a, b), для каждого элемента list.
		/// </summary>
		/// <param name="array">Array.</param>
		/// <param name="compareDelegate">(a, b)</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static T[] Sort<T>(T[] array, DCompareItem<T> compareDelegate)
		{
			bool replace = true;
			
			T[] tmp = Copy<T>(array, 0, array.Length);
			
			while (replace) {
				replace = false;
				for (int i=0; i<tmp.Length-1; i++) {
					if (compareDelegate.Invoke (tmp[i], tmp[i+1]) > 0) {
						Swap<T>(ref tmp[i], ref tmp[i+1]);
						replace = true;
					}
				}
			}
			return tmp;
		}
		
		/// <summary>
		/// Вернет отсортированную копию list, сортировка будет проведена по значению, возвращенному 
		/// функцией compareDelegate(a, b), для каждого элемента list.
		/// </summary>
		/// <param name="array">Array.</param>
		/// <param name="compareDelegate">(a, b)</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static void Sort<T>(ref T[] array, DCompareItem<T> compareDelegate)
		{
			bool replace = true;
			while (replace) {
				replace = false;
				for (int i=0; i<array.Length-1; i++) {
					if (compareDelegate.Invoke (array[i], array[i+1]) > 0) {
						T item = array [i];
						array[i] = array[i +1];
						array[i + 1] = item;
						replace = true;
					}
				}
			}
		}
		
		/// <summary>
		/// Вернет позицию, на которой находится элемент value в массиве array, или -1, 
		/// если данный элемент не был найден.
		/// </summary>
		/// <returns>The of.</returns>
		/// <param name="array">Array.</param>
		/// <param name="item">Item.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static int IndexOf<T>(T[] array, T item)
		{
			return Array.IndexOf(array, item);
		}
		
		/// <summary>
		/// Вернет последний элемент массива array.
		/// </summary>
		/// <returns>The index of.</returns>
		/// <param name="array">Array.</param>
		/// <param name="item">Item.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static int LastIndexOf<T>(T[] array, T item)
		{
			return Array.LastIndexOf(array, item);
		}
		
		
		
		/// <summary>
		/// Вернет первый элемент массива array. 
		/// </summary>
		/// <param name="array">Array.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static T First<T>(T[] array)
		{
			if (array.Length > 0)
				return array[0];
			return default(T);
		}
		
		/// <summary>
		/// Вернет первый элемент массива array. Если был передан аргумент n, 
		/// то будет возвращен подмассив из n первых элементов родительского.
		/// </summary>
		/// <param name="array">Array.</param>
		/// <param name="count">Count.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static T[] First<T>(T[] array, int count)
		{
			count = Math.Min (array.Length, count);
			return Copy<T>(array, 0, count);
		}
		
		/// <summary>
		/// Вернет последний элемент массива array. Если был передан аргумент n, 
		/// то будет возвращен подмассив из n последних элементов родительского.
		/// </summary>
		/// <param name="array">Array.</param>
		/// <param name="count">Count.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static T[] Last<T>(T[] array, int count)
		{
			count = Math.Min (array.Length, count);
			return Copy<T>(array, string.Format("-{0}:", count ));
		}
		
		/// <summary>
		/// Вернет последний элемент массива array.
		/// </summary>
		/// <param name="array">Array.</param>
		/// <param name="item">Item.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static int Last<T>(T[] array, T item)
		{
			return LastIndexOf(array, item);
		}
		
		/// <summary>
		/// Разделяет array, на два массива arrayA, arrayB по индексу index.
		/// </summary>
		/// <param name="array">Array.</param>
		/// <param name="arrayA">Array a.</param>
		/// <param name="arrayB">Array b.</param>
		/// <param name="index">Index.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static void Devide<T>(T[] array, out T[] arrayA, out T[] arrayB, int index)
		{
			if (index >= array.Length){
				arrayA = array;
				arrayB = new T[0];
				return;
			}
			
			if (index <= 0) {
				arrayB = array;
				arrayA = new T[0];
				return;
			}
			
			arrayA = new T[index];
			arrayB = new T[array.Length - index];
			
			Array.Copy (array, 0, arrayA, 0, arrayA.Length);
			Array.Copy (array, index, arrayB, 0, arrayB.Length);
		}
		
		/// <summary>
		/// Разделяет array, на два массива arrayA, arrayB пополам (округляя в меншую сторону)
		/// </summary>
		/// <param name="array">Array.</param>
		/// <param name="arrayA">Array a.</param>
		/// <param name="arrayB">Array b.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static void Devide<T>(T[] array, out T[] arrayA, out T[] arrayB)
		{
			Devide<T>(array, out arrayA, out arrayB, (int)(array.Length / 2));
		}
		
		/// <summary>
		/// Упаковывает 2-ва одномерных массива в 2-вумерный.
		/// </summary>
		/// <param name="arrayA">Array a.</param>
		/// <param name="arrayB">Array b.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static T[,] Zip<T>(T[] arrayA, T[] arrayB)
		{
			if (arrayA.Length != arrayB.Length)
				throw new ArgumentOutOfRangeException("arrayA.Length != arrayB.Length");
			T[,] res = new T[arrayA.Length, 2];
			for (int i=0; i<arrayA.Length; i++) {
				res[i,0] = arrayA[i];
				res[i,1] = arrayB[i];
			}
			return res;
		}
		
		
		#region Insert, Add
		
		public static T[] Insert<T>(T[] array, T item, int position)
		{
			T[] tmp = new T[array.Length+1];
			
			T[] a;
			T[] b;
			
			Devide(array, out a, out b, position);
			
			tmp [position] = item;
			Array.Copy(a, 0, tmp, 0, a.Length);
			Array.Copy(b, 0, tmp, position + 1, b.Length);
			
			return tmp;
			
		}
		
		public static void Insert<T>(ref T[] array, T item, int position)
		{
			T[] tmp = new T[array.Length+1];
			
			T[] a;
			T[] b;
			
			Devide(array, out a, out b, position);
			
			tmp [position] = item;
			Array.Copy(a, 0, tmp, 0, a.Length);
			Array.Copy(b, 0, tmp, position + 1, b.Length);
			
			array = tmp;
			
		}
		
		
		public static T[] Insert<T>(T[] array, T[] items, int position)
		{
			T[] tmp = new T[array.Length+items.Length];
			
			T[] a,b;
			
			Devide(array, out a, out b, position);
			Array.Copy(a, 0, tmp, 0, a.Length);
			Array.Copy(items, 0, tmp, position, items.Length);
			Array.Copy(b, 0, tmp, position + items.Length, b.Length);
			
			return tmp;
			
		}
		
		public static void Insert<T>(ref T[] array, T[] items, int position)
		{
			T[] tmp = new T[array.Length+items.Length];
			
			T[] a,b;
			
			Devide(array, out a, out b, position);
			Array.Copy(a, 0, tmp, 0, a.Length);
			Array.Copy(items, 0, tmp, position, items.Length);
			Array.Copy(b, 0, tmp, position + items.Length, b.Length);
			
			array = tmp;
		}
		
		public static void Add<T>(ref T[] array, T item)
		{
			array = Add (array, item);
		}
		
		public static T[] Add<T>(T[] array, T item)
		{
			return Insert (array, item, array.Length);
		}
		
		#endregion
		
		#region Remove
		
		/// <summary>
		/// получить первый элемент массива и усеч его на 1
		/// </summary>
		/// <param name="array">Array.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static T Pop<T>(ref T[] array)
		{
			T[] tmp = new T[array.Length - 1];
			Array.Copy (array, 1, tmp, 0, tmp.Length);
			T res = array[0];
			array = tmp;
			return res;
		}
		
		/// <summary>
		/// Получить последний элемент массива и усеч его на 1
		/// </summary>
		/// <param name="array">Array.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static T Shift<T>(ref T[] array)
		{
			T[] tmp = new T[array.Length - 1];
			Array.Copy (array, 0, tmp, 0, tmp.Length);
			T res = array[tmp.Length];
			array = tmp;
			return res;
		}
		
		/// <summary>
		/// Удаляет из массива элементы создавая копию
		/// </summary>
		/// <returns>The from.</returns>
		/// <param name="array">Array.</param>
		/// <param name="startIndex">Start index.</param>
		/// <param name="count">Count.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static T[] RemoveFrom<T>(T[] array, int startIndex, int count)
		{
			
			if (startIndex > array.Length)
				return array;
			
			if (startIndex + count > array.Length)
				count = array.Length - startIndex;
			
			T[] tmp = new T[array.Length - count];
			Array.Copy (array, 0, tmp, 0, startIndex);
			Array.Copy (array, startIndex + count, tmp, startIndex, array.Length - (startIndex + count));
			return tmp;
		}
		
		/// <summary>
		/// Удаляет из массива элементы
		/// </summary>
		/// <param name="array">Array.</param>
		/// <param name="startIndex">Start index.</param>
		/// <param name="count">Count.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static void RemoveFrom<T>(ref T[] array, int startIndex, int count)
		{
			array = RemoveFrom(array, startIndex, count);
		}
		
		#endregion
		
		#region Copy
		
		/// <summary>
		/// Создает копию массива
		/// </summary>
		/// <param name="array">Array.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static T[] Copy<T>(T[] array)
		{
			return Copy (array, 0, array.Length);
		}
		
		/// <summary>
		/// Cоздает копию массива с позиции start до позиции end,
		/// если start = null то с начала массива, 
		/// если end = null то до конца массива,
		/// елси start/end отричательный то от конча массива отступить n элементов
		/// </summary>
		/// <param name="array">Array.</param>
		/// <param name="start">Start.</param>
		/// <param name="end">End.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static T[] Copy<T>(T[] array, int? start, int? end)
		{
			if (start != null) {
				if (start < 0)
					start = array.Length + start;
			} else {
				start = 0;
			}
			
			if (end != null) {
				if (end < 0)
					end = array.Length + end;
			} else {
				end = array.Length;
			}
			if (start > end)
				return new T[0];
			T[] tmp = new T[(int)end - (int)start];
			Array.Copy(array, (int)start, tmp, 0, tmp.Length);
			return tmp;
		}
		
		/// <summary>
		/// Возвращает усеченный массив с позиции start до позиции end,
		/// если start = null то с начала массива, 
		/// если end = null то до конца массива,
		/// елси start/end отричательный то от конча массива отступить n элементов
		/// </summary>
		/// <param name="array">Array.</param>
		/// <param name="start">Start.</param>
		/// <param name="end">End.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static void Copy<T>(ref T[] array, int? start, int? end)
		{
			array = Copy (array, start, end);
		}
		
		/// <summary>
		/// Cоздает копию массива по правилу pythonStringArray (1:-1)
		/// </summary>
		/// <param name="array">Array.</param>
		/// <param name="pythonArrayString">Python array string.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static void Copy<T>(ref T[] array, string pythonArrayString)
		{
			array = Copy (array, pythonArrayString);
		}
		
		/// <summary>
		/// Усекает саммив по правилу pythonStringArray (1:-1)
		/// </summary>
		/// <param name="array">Array.</param>
		/// <param name="pythonArrayString">Python array string.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static T[] Copy<T>(T[] array, string pythonArrayString)
		{
			int? start = null;
			int? end = null;
			if (pythonArrayString.IndexOf (":") > -1) {
				string[] args = pythonArrayString.Split (':');
				if (args [0] != "")
					start = int.Parse (args[0]);
				if (args [1] != "")
					end = int.Parse (args[1]);
				return Copy (array, start, end);
			} else {
				int index = int.Parse (pythonArrayString);
				
				if (index < 0)
					index = array.Length + index;
				
				if (index < 0)
					return new T[0];
				if (index >= array.Length)
					return new T[0];
				
				return new T[] { array[index] };
			}
		}
		
		#endregion 
		
		#region Renge
		
		/// <summary>
		/// Создает массив последовательных элементов между start и end с шагом step
		/// </summary>
		/// <param name="start">Start.</param>
		/// <param name="end">End.</param>
		/// <param name="step">Step.</param>
		public static int[] Range(int start, int end, int step)
		{
			int[] t = new int[0];
			for (int i=start; i<end; i+=step) {
				Add (ref t, i);
			}
			return t;
		}
		
		/// <summary>
		/// Создает массив последовательных элементов между start и end с шагом 1
		/// </summary>
		/// <param name="start">Start.</param>
		/// <param name="end">End.</param>
		public static int[] Range(int start, int end)
		{
			return Range (start, end, 1);
		}
		
		/// <summary>
		/// Создает массив последовательных элементов от нуля до end с шагом 1
		/// </summary>
		/// <param name="end">End.</param>
		public static int[] Range(int end)
		{
			return Range(0, end, 1);
		}
		
		/// <summary>
		/// Создает перечисление между start и end с шагом step
		/// </summary>
		/// <returns>The range.</returns>
		/// <param name="start">Start.</param>
		/// <param name="end">End.</param>
		/// <param name="step">Step.</param>
		public IEnumerator<int> XRange(int start, int end, int step)
		{
			for (int i=start; i<end; i+=step) {
				yield return i;
			}
		}
		
		/// <summary>
		/// Создает перечисление между start и end с шагом 1
		/// </summary>
		/// <returns>The range.</returns>
		/// <param name="start">Start.</param>
		/// <param name="end">End.</param>
		public IEnumerator<int> XRange(int start, int end)
		{
			return XRange(start, end, 1);
		}
		
		/// <summary>
		/// Создает перечисление между 0 и end с шагом 1
		/// </summary>
		/// <returns>The range.</returns>
		/// <param name="end">End.</param>
		public IEnumerator<int> XRange(int end)
		{
			return XRange(0, end, 1);
		}
		
		#endregion
		
		#region Objects
		
		/// <summary>
		/// Получить значение всех свойств объекта
		/// </summary>
		/// <param name="obj">Object.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static string[] Keys<T>(T obj)
		{
			List<string> keys = new List<string> ();
			foreach (System.Reflection.PropertyInfo info in obj.GetType().GetProperties())
				keys.Add (info.Name);
			return keys.ToArray ();
		}
		
		/// <summary>
		/// Получить значения всех свойств объекта
		/// </summary>
		/// <param name="obj">Object.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static object[] Values<T>(T obj)
		{
			List<object> vals = new List<object> ();
			foreach (System.Reflection.PropertyInfo info in obj.GetType().GetProperties()) {
				try{
					vals.Add (info.GetValue(obj,null));
				}catch{
				}
			}
			return vals.ToArray ();
		}
		
		/// <summary>
		/// Получить имена методов объекта
		/// </summary>
		/// <param name="obj">Object.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static string[] Functions<T>(T obj)
		{
			List<string> keys = new List<string> ();
			foreach (System.Reflection.MethodInfo info in obj.GetType().GetMethods()) {
				if ((info.IsPublic)&&(!info.IsStatic))
					keys.Add (info.Name);
			}
			
			return keys.ToArray();
		}
		
		/// <summary>
		/// Список интерфейсов
		/// </summary>
		/// <param name="obj">Object.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static string[] Interfaces<T>(T obj)
		{
			string[] r = new string[0];
			foreach(Type t in obj.GetType().GetInterfaces())
				Add (ref r, t.Name);
			return r;
		}
		
		/// <summary>
		/// Определить подходит ли данный Type к текущему объекту
		/// </summary>
		public static bool ReleaseInterface<T>(T obj, System.Type type)
		{
			return ( (Find ( obj.GetType().GetInterfaces(), (item) => { return item.GetType() == type; }) != null) || 
			        (Find ( obj.GetType().GetNestedTypes(), (item) => { return item.GetType() == type; }) != null ));
		}
		
		
		
		/// <summary>
		/// Проверяет, есть ли свойство key в объекте object
		/// </summary>
		/// <returns><c>true</c> if has obj name; otherwise, <c>false</c>.</returns>
		/// <param name="obj">Object.</param>
		/// <param name="name">Name.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static bool Has<T>(T obj, string name)
		{
			foreach (System.Reflection.PropertyInfo info in obj.GetType().GetProperties())
				if (info.Name == name)
					return true;
			return false;
		}
		
		#endregion
		
		#region Union and Intersection
		
		/// <summary>
		/// Объеденит уникальные элементы всех массивов arrays. Порядок элементов будет определен 
		/// порядком их появления в исходных массивах.
		/// </summary>
		/// <param name="arrays">Arrays.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static T[] Union<T>(params object[] arrays)
		{
			List<T> flat = new List<T>();
			
			foreach(object array in arrays)
			{
				foreach (T item in (T[])array) {
					if (flat.IndexOf (item) == -1)
						flat.Add (item);
				}
			}
			return flat.ToArray ();
			
		}
		
		/// <summary>
		/// Вернет массив из элементов, встречающихся в каждом из переданных массивов array1 и array2.
		/// </summary>
		/// <param name="array1">Array1.</param>
		/// <param name="array2">Array2.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static T[] Intersection<T>(T[] array1, T[] array2)
		{
			var result = new List<T>();
			foreach (T i1 in array1)
			{
				foreach (T i2 in array2)
				{
					if (i1.Equals(i2))
					{
						if (!result.Contains(i1))
							result.Add(i1);
						break;
					}
				}
			}
			return result.ToArray();
		}
		
		
		/// <summary>
		/// Вернет массив из элементов, встречающихся в каждом из переданных массивов arrays
		/// </summary>
		/// <param name="array1">Array1.</param>
		/// <param name="array2">Array2.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static T[] Intersection<T>(params object[] arrays)
		{
			if (arrays.Length == 1)
				return (T[])arrays[0];
			
			T[] res = (T[])arrays[0];
			for(int i=1;i<arrays.Length;i++)
				res = Intersection<T>(res, (T[])arrays[i]);
			return res;
		}
		
		#endregion
		
		#region utils
		
		/// <summary>
		/// Выполнить указанную функцию count раз, при каждом выполнении будет передан номер итерации
		/// </summary>
		/// <param name="count">Count.</param>
		/// <param name="indexer">(iteration).</param>
		public static void Times(int count, DIterator indexer)
		{
			for (int i=0; i<count; i++)
				indexer.Invoke (i);
		}
		
		/// <summary>
		/// Выполнить указанную функцию count раз, при каждом выполнении будет передан номер итерации и контекст.
		/// </summary>
		/// <param name="count">Count.</param>
		/// <param name="context">Context.</param>
		/// <param name="indexer">Indexer.</param>
		public static void Times(int count, object context, DIteratorContext indexer)
		{
			for (int i=0; i<count; i++)
				indexer.Invoke (i, context);
		}
		
		/// <summary>
		/// Создать уникальный идентификатор
		/// </summary>
		/// <returns>The identifier.</returns>
		public static string UniqueId()
		{
			return Guid.NewGuid().ToString("N");
		}
		
		/// <summary>
		/// Создать уникальный идентификатор
		/// </summary>
		/// <returns>The identifier.</returns>
		/// <param name="prefix">Prefix.</param>
		public static string UniqueId(string prefix)
		{
			return prefix+Guid.NewGuid().ToString("N");
		}
		
		#endregion
	}
	
}

