﻿// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by a tool.
//      Mono Runtime Version: 4.0.30319.1
// 
//      Changes to this file may cause incorrect behavior and will be lost if 
//      the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------
using System;
using UnityEngine;
using System.Collections.Specialized;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Реализует доступ к данным через AfApi
/// </summary>
using Ionic.Zlib;


public class AfApi3: MonoBehaviour
{
	
	public delegate void DAfApiResponse(AfApi3Response apiresponse);

	private Queue<AfApi3.AfApi3Request> requestsQueue = new Queue<AfApi3Request>();
	private bool busy = false;

	//public event DAfApiResponse OnAfApi3Response;

	public static AfApi3 Instance {get; protected set;}

	/// <summary>
	/// The host.
	/// </summary>
	public string host = "http://192.168.1.7:8080/api/";
	
	/// <summary>
	/// Реализует запрос к серверу
	/// </summary>
	public class AfApi3Request
	{
		private DateTime requestTime = DateTime.Now;

		private int timeout = 6000;

		public enum AfApi3HttpCompress
		{
			Uncompressed,
			GZipBase64,
			GZip,
			Deflate
		}

		public AfApi3HttpCompress SendCompress {get; set; }
		public AfApi3HttpCompress ReceiveCompress {get; set; }

		public int Timeout { get { return timeout; } set { timeout = value; } }

		public bool IsTimeouted
		{
			get{
				return DateTime.Now.Subtract(requestTime).TotalMilliseconds > timeout;
			}
		}

		public enum AfApi3HttpMethod
		{
			GET,
			POST
		}
		
		private static int lastId = UnityEngine.Random.Range(0, 10000);
		public string Method {get; set;}
		public Dictionary<string, object> Args {get; protected set; }
		public int Id { get; set; }
		public AfApi3HttpMethod HttpMethod = AfApi3HttpMethod.POST;
		public string Text { get; set; }
		public DAfApiResponse Callback {get; set;}
		public DAfApiResponse ErrorCallback {get; set;} 
		
		public string ToUrlArgs()
		{
			if (this.Args == null) this.Args = new Dictionary<string, object>();
			List<string> l = new List<string>();
			foreach(string key in  this.Args.Keys)
				l.Add(string.Format("{0}={1}", key, System.Uri.EscapeDataString(this.Args[key].ToString()) ) );
			return Method + "/?" + string.Join("&", l.ToArray());
		}

		private byte[] GZip(byte[] a)
		{
			var ms = new System.IO.MemoryStream();
			using (var compressor = new Ionic.Zlib.GZipStream(ms,  CompressionMode.Compress, CompressionLevel.Default ) ) 
			{
				compressor.Write(a ,0, a.Length);
			}
			return ms.ToArray();
		}

		private string GZip(string a)
		{
			return System.Text.Encoding.UTF8.GetString( GZip(System.Text.Encoding.UTF8.GetBytes(a)) );
		}

		private string GZipBase64(string a)
		{
			Debug.LogWarning("Zipping....");
			return System.Convert.ToBase64String(GZip(System.Text.Encoding.Default.GetBytes(a)));
		}

		public string ToPOST()
		{
			string res = "";
			if (this.Text == "")
			{
				res = "{";

				List<string> gs = new List<string>();
				foreach(string key in Args.Keys)
				{
					if (Args[key].GetType() == typeof(int))
						gs.Add(string.Format("\"{0}\":{1}", key, Args[key]));  
					if (Args[key].GetType() == typeof(string))
						gs.Add(string.Format("\"{0}\":\"{1}\"", key, Args[key]));  
					if (Args[key].GetType() == typeof(string[]) || Args.Keys.GetType() == typeof(int[]) || Args.Keys.GetType() == typeof(float[]) || Args.Keys.GetType() == typeof(double[]))
						gs.Add(string.Format("\"{0}\":{1}", key, LitJson.JsonMapper.ToJson(Args[key]) ));  
					if (Args[key].GetType() == typeof(LitJson.JsonData))
						gs.Add(string.Format("\"{0}\":{1}", key, ((LitJson.JsonData)Args[key]).ToJson() ));  
				}
				
				res += String.Join(",",gs.ToArray());
				
				res += "}";
			}
			else 
				res = this.Text;

			//Debug.LogWarning(string.Format("{2}  Compress is:{0} {1}", this.SendCompress, this.SendCompress == AfApi3HttpCompress.GZip, this.Method));


			if (this.SendCompress == AfApi3HttpCompress.GZipBase64)
				return GZipBase64(res);
			else
				return res;
		}

		public AfApi3Request(string method, Dictionary<string, object> args)
		{
			this.Text = "";
			this.Method = method;
			this.Args = args;
			this.Id = lastId;
			lastId ++;
			this.SendCompress = AfApi3HttpCompress.Uncompressed;
		}
		
		public AfApi3Request(string method):this(method, new Dictionary<string, object>()) {}
	}
	
	/// <summary>
	/// Реализует ответ от сервера
	/// </summary>
	public class AfApi3Response
	{
		public class AfApi3Result
		{
			public string Result {get; protected set;}
			private LitJson.JsonData _toJson = null;
			private LitJson.JsonData _toJsonError = null;
			private LitJson.JsonData _toJsonResult = null;

			private SimpleJSON.JSONNode _toSimpleJsonResult = null;
			private SimpleJSON.JSONNode _toSimpleJson = null;

			public SimpleJSON.JSONNode ToSimpleJson() { 
				if (_toSimpleJson == null)
					_toSimpleJson = SimpleJSON.JSON.Parse(this.Result);
				return _toSimpleJson;
			}

			public LitJson.JsonData ToJson() { 

				if (_toJson == null)
					this._toJson = LitJson.JsonMapper.ToObject(this.Result); 
				return this._toJson;
			}

			public LitJson.JsonData JsonError
			{
				get 
				{
					LitJson.JsonData data = ToJson();
					try{
						_toJsonError = data["error"];
					}
					catch{
						_toJsonError = null;
					}
					return _toJsonError;
				}
			
			}

			public SimpleJSON.JSONNode SimpleJsonResult
			{
				get {
					try{
						_toSimpleJsonResult = SimpleJSON.JSON.Parse(this.Result)["result"];
					}
					catch{
						_toSimpleJsonResult = null;
					}
					return _toSimpleJsonResult;
				}
			}

			public LitJson.JsonData JsonResult
			{
				get {
					LitJson.JsonData data = ToJson();
					try{
						_toJsonResult = data["result"];
					}
					catch{
						_toJsonResult = null;
					}
					return _toJsonResult;
				}
			}

			public AfApi3Result(string result) { this.Result = result; }
		}
		
		public int Id {get; protected set;}
		public AfApi3Result Result { get; protected set;}
		public string Error { get; protected set; }
		public int StatusCode {get; protected set;}

		private void SendCallback(DAfApiResponse callback)
		{
			try
			{
				callback.Invoke(this);
			}catch(Exception e)
			{
				UnityEngine.Debug.LogError("CallbackError:"+e.Message);
				UnityEngine.Debug.LogException(e);
			}
		}

		public void SendCallback(AfApi3Request reqs)
		{
			if (reqs.Callback != null)
				SendCallback(reqs.Callback);
		}

		public void SendErrorCallback(AfApi3Request reqs)
		{

			if (reqs.ErrorCallback != null)
				SendCallback(reqs.ErrorCallback);
			else
				SendCallback(reqs.Callback);
		}

		
		public AfApi3Response(string text)
		{
			this.Result = new AfApi3Result(text);
			this.StatusCode = 200;
		}

		public AfApi3Response(int code) { 
			this.StatusCode = code;
			this.Result = new AfApi3Result("");
		}
		
		public AfApi3Response(int code, string text)
		{
			this.StatusCode = code;
			this.Result = new AfApi3Result(text);
		}

		public static AfApi3Response GetTimeoutResponse()
		{
			AfApi3Response r = new AfApi3Response(401);
			r.Result = new AfApi3Result("");
			r.Error = "timeout";
			return r;
		}
	}
	
	private IEnumerator _send(AfApi3Request apirequest)
	{
		busy = true;
		string url =  this.host;
		HTTP.Request webrequest;

		if (apirequest.HttpMethod == AfApi3Request.AfApi3HttpMethod.GET)
		{
			url =  this.host + apirequest.ToUrlArgs();
			//Debug.Log("GET URL:"+url);
			webrequest = new HTTP.Request("GET", url);
		}
		else
		{
			url = this.host + apirequest.Method + "/";

			webrequest = new HTTP.Request("POST", url);
			webrequest.Text = apirequest.ToPOST();

			if (apirequest.SendCompress == AfApi3Request.AfApi3HttpCompress.GZipBase64)
				url += "?compressed=gzip-base64";
			if (apirequest.SendCompress == AfApi3Request.AfApi3HttpCompress.GZip)
				url += "?compressed=gzip";


			Debug.Log("POST URL:"+url);
			Debug.Log("POST TEXT:" + apirequest.ToPOST());
		}
		webrequest.Send();
		while (!webrequest.isDone) 
		{
			yield return new WaitForEndOfFrame();
		}
		busy = false;
		AfApi3Response apiresponse;
		if (webrequest.exception == null)
		{
			try {
				HTTP.Response webresponse = webrequest.response;
				Debug.Log("Status:" + webresponse.status.ToString());
				switch(webresponse.status)
				{
				case 200:
				case 201:
					Debug.Log("OK:"+webresponse.Text);
					apiresponse = new AfApi3Response(webresponse.Text);
					apiresponse.SendCallback(apirequest);
					break;
				default:
					apiresponse = new AfApi3Response(webresponse.status);
					apiresponse.SendErrorCallback(apirequest);
					break;
				}
			}catch(Exception e)
			{
				Debug.LogError(string.Format("Error in webresponse:{0}", e.Message));
				apiresponse = new AfApi3Response(505);
				apiresponse.SendErrorCallback(apirequest);
			}

		} else {
			try{
				Debug.LogError(string.Format("WEB EXCEPTION: {0}", webrequest.exception));
				apiresponse = new AfApi3Response(500);
				apiresponse.SendErrorCallback(apirequest);
			} catch {
				Debug.LogError(string.Format("WEB EXCEPTION IS NULL"));
				apiresponse = new AfApi3Response(500);
				apiresponse.SendErrorCallback(apirequest);
			}
		}
	}

	private int TryToSend(AfApi3Request apirequest, DAfApiResponse callback, DAfApiResponse error_callback)
	{
		apirequest.Callback = callback;
		apirequest.ErrorCallback = error_callback;
		this.requestsQueue.Enqueue(apirequest);
		return apirequest.Id;
	}

	public int Send(AfApi3Request apirequest, DAfApiResponse callback)
	{
		return Send(apirequest, callback, null);
	}
	public int Send(AfApi3Request apirequest, DAfApiResponse callback, DAfApiResponse error_callback)
	{
		//StartCoroutine(_send(apirequest, callback, error_callback));
		//return apirequest.Id;
		return TryToSend(apirequest, callback, error_callback);
	}

	public int Send(AfApi3Request apirequest, string method, DAfApiResponse callback)
	{
		return Send(apirequest, method, callback, null);
	}

	public int Send(AfApi3Request apirequest, string method, DAfApiResponse callback, DAfApiResponse error_callback)
	{
		switch(method){
		case "GET":
			apirequest.HttpMethod = AfApi3Request.AfApi3HttpMethod.GET;
			break;
		case "POST":
			apirequest.HttpMethod = AfApi3Request.AfApi3HttpMethod.POST;
			break;
		default:
			break;
		}
		return TryToSend(apirequest, callback, error_callback);
		//StartCoroutine(_send(apirequest, callback, error_callback));
		//return apirequest.Id;
	}
	
	public int Send(string method, DAfApiResponse callback) { return Send(new AfApi3Request(method), callback); }

	void Awake() {
		Instance = this;
	}

	void Start() { }
	
	void Update() {
		if(!busy && requestsQueue.Count > 0)
		{
			AfApi3Request r = requestsQueue.Dequeue();
			if (!r.IsTimeouted)
				StartCoroutine(_send(r));
			else
			{
				if (r.ErrorCallback!= null) 
					r.ErrorCallback.Invoke(AfApi3Response.GetTimeoutResponse() );
				else
					r.Callback.Invoke(AfApi3Response.GetTimeoutResponse() );
			}
			
		}
	}
}