﻿using UnityEngine;
using System.Collections;
using System;

public enum Languages
{
	RUS,
	KZ,
	EN,
	UA,
	ESP
}

[Serializable]
public class LocalizationString
{
	public Languages lang = Languages.RUS;
	public string newLine;
	public float scale = 1.0f;
	public LocalizationString() {}
	public LocalizationString(Languages lang, string line)
	{
		this.lang = lang;
		this.newLine = line;
	}
}

