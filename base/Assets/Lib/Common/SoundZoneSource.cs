﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class SoundZoneSource : MonoBehaviour {

	AudioSource src;

	private Queue<AudioClip> clips = new Queue<AudioClip>();

	// Use this for initialization
	void Start () {
	}
	

	public void Play(AudioClip clip)
	{
		this.src = this.gameObject.AddComponent<AudioSource>();
		this.clips.Enqueue(clip);
	}

	public void Play(AudioClip[] clips)
	{
		this.src = this.gameObject.AddComponent<AudioSource>();
		foreach(AudioClip clip in clips)
			this.clips.Enqueue(clip);

	}

	// Update is called once per frame
	void Update () {
		if (!src.isPlaying && clips.Count > 0)
		{
			this.src.clip = clips.Dequeue();
			//this.src.volume = this.volume;
			this.src.Play();

		}
		else if (!src.isPlaying && clips.Count == 0)
		{
			Destroy(src);
			Destroy(this.gameObject);
		}
	}
}
