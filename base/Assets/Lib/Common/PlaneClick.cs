﻿using UnityEngine;
using System.Collections;

public class PlaneClick : MonoBehaviour {

	public GameObject target;
	public string message = "";

	public void OnMouseUp()
	{
		if (target != null)
			target.SendMessage(message);
		else
			Debug.LogError("Target not found");
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
