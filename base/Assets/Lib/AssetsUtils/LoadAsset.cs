﻿

using UnityEngine;
using System.Collections;
using System.IO;public class LoadAsset : MonoBehaviour {
	
	public string AssetName = "RouletteFlat";
	
	IEnumerator Start() {
		
		Debug.Log(string.Format("{0}", System.IO.Directory.GetCurrentDirectory()));
		
		
		byte[] data = Read ("./bundle/RouletteFlat.unity3d");  // System.IO.File.ReadAllBytes("./bin/Assets/test1.unity3d");
		
		
		Debug.Log(string.Format("{0}", data.Length ));
		AssetBundleCreateRequest acr = AssetBundle.CreateFromMemory(data);
		yield return acr;
		AssetBundle bundle = acr.assetBundle;
		if (AssetName == "")
			Instantiate(bundle.mainAsset);
		else
			Instantiate(bundle.Load(AssetName));
		bundle.Unload(false);
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	byte[] Read(string path)
	{
		MemoryStream dest = new MemoryStream();
		using(Stream source = File.OpenRead(path)) {
			byte[] buffer = new byte[2048];
			int bytesRead;
			while((bytesRead = source.Read(buffer, 0, buffer.Length)) > 0) {
				dest.Write(buffer, 0, bytesRead);
			}
		}
		return dest.ToArray();
	}
	
	
}
