﻿// Builds an asset bundle from the selected objects in the project view,
// and changes the texture format using an AssetPostprocessor.

#if UNITY_EDITOR

using UnityEngine;
using UnityEditor;

public static class ExportAssetBundles {
	
	public static TextureImporterFormat textureFormat;

	
	public static void GetPrefab()
	{
		Debug.Log(System.IO.Directory.GetCurrentDirectory().ToString());
		string appname = "RouletteFlat";
		if (!System.IO.Directory.Exists("bundle"))
			System.IO.Directory.CreateDirectory("bundle");

		foreach(string command in System.Environment.GetCommandLineArgs())
		{
			string[] cmd = command.Split(':');
			if (cmd[0] == "-appname")
				appname = cmd[1];
		}

		Debug.Log(appname);
		textureFormat = TextureImporterFormat.PVRTC_RGB4;
		string localPath = string.Format("Assets/_SRC/{0}/prefab/{0}.prefab", appname);
		Object go = AssetDatabase.LoadAssetAtPath(localPath, typeof(Object));
		BuildPipeline.BuildAssetBundle(go, new Object[] { go }, string.Format("bundle/{0}.unity3d", appname), BuildAssetBundleOptions.CollectDependencies | BuildAssetBundleOptions.CompleteAssets);
	}
}

#endif