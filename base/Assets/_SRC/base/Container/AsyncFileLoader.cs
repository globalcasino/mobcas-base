// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by a tool.
//      Mono Runtime Version: 4.0.30319.1
// 
//      Changes to this file may cause incorrect behavior and will be lost if 
//      the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------
using System;
using System.IO;

namespace mobcas.Container
{
	public class AsyncFileLoader
	{

		public static byte[] Read(string path)
		{
			MemoryStream dest = new MemoryStream();
			using(Stream source = File.OpenRead(path)) {
				byte[] buffer = new byte[2048];
				int bytesRead;
				while((bytesRead = source.Read(buffer, 0, buffer.Length)) > 0) {
					dest.Write(buffer, 0, bytesRead);
				}
			}
			return dest.ToArray();
		}

	}
}

