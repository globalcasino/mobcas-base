﻿using UnityEngine;
using System.Collections;

namespace mobcas.Container {

	public class ContainerLoader : MonoBehaviour {

		public static string fileLoad = "";
		public static string assetName = "";

		public static ContainerLoader Instance { get; protected set; }

		private AssetBundle bundle;

		void Awake()
		{
			Instance = this;
		}

		// Use this for initialization
		void Start () {
			if (fileLoad == "")
				ToLobby();
			else
				StartCoroutine("Load");
		}

		private IEnumerator Load()
		{
			Debug.Log("Open File:" + fileLoad);
			byte[] data = AsyncFileLoader.Read(fileLoad);
			Debug.Log("DataSize:"+data.Length.ToString());
			AssetBundleCreateRequest acr = AssetBundle.CreateFromMemory(data);
			yield return acr;
			Debug.Log("Create ACR");
			AssetBundle bundle = acr.assetBundle;

			if (assetName == "")
				Instantiate(bundle.mainAsset);
			else
				Instantiate(bundle.Load(assetName));
			bundle.Unload(false);
		}

		public void ToLobby()
		{
			Unload ();
			Application.LoadLevel(MainSettings.LobbyName);
		}

		private void Unload()
		{
			if (bundle != null)
			{
				bundle.Unload(true);
				bundle = null;
			}
		}



		// Update is called once per frame
		void Update () {
		
		}
	}

}